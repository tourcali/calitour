export default class User {
  constructor(nombre, apellido, correo, user, password, rol) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.correo = correo;
    this.user = user;    
    this.password = password;
    this.rol = rol
  }
}
