import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import Menu from './views/Menu.vue';
import Email from './views/Email.vue';
import Inactive from './views/Inactive.vue';
import Registersite from './views/Registersite.vue';
import Deletesite from './views/Deletesite.vue';
import Consultsite from './views/Consultsite.vue';
import Menusite from './views/Menusite.vue';
import Eliminar from './views/Eliminar';


Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/menu',
      component: Menu
    },
    {
      path: '/email',
      component: Email
    },
    {
      path: '/inactive',
      component: Inactive
    },
    {
      path: '/registersite',
      component: Registersite
    },
    {
      path: '/deletesite',
      component: Deletesite
    },
    {
      path: '/consultsite',
      component: Consultsite
    },
    {
      path: '/menusite',
      component: Menusite
    },

    {
      path: '/eliminar',
      component: Eliminar
    },

    {
      path: '/menu',
      name: 'menu',
      // lazy-loaded
      component: () => import('./views/Menu.vue')
    },
    {
      path: '/email',
      name: 'email',
      // lazy-loaded
      component: () => import('./views/Email.vue')
    },
    {
      path: '/inactive',
      name: 'inactive',
      // lazy-loaded
      component: () => import('./views/Inactive.vue')
    },
    {
      path: '/registersite',
      name: 'registersite',
      // lazy-loaded
      component: () => import('./views/Registersite.vue')
    },
    {
      path: 'deletesite',
      name: 'Deletesite',
      // lazy-loaded
      component: () => import('./views/Deletesite.vue')
    },

    {
      path: 'Consultsite',
      name: 'Consultsite',
      // lazy-loaded
      component: () => import('./views/Consultsite.vue')
    },
    {
      path: '/Menusite',
      name: 'Menusite',
      // lazy-loaded
      component: () => import('./views/Menusite.vue')
    },
    {
      path: '/admin',
      name: 'admin',
      // lazy-loaded
      component: () => import('./views/BoardAdmin.vue')
    },
    {
      path: '/mod',
      name: 'moderator',
      // lazy-loaded
      component: () => import('./views/BoardModerator.vue')
    },
    {
      path: '/user',
      name: 'user',
      // lazy-loaded
      component: () => import('./views/BoardUser.vue')
    }
  ]
});
