import axios from 'axios';

const API_URL = 'http://localhost:9092/';

class AuthService {
  login(user) {
    return axios.post(API_URL + 'logear', {
        user: user.user,
        password: user.password
      })
      .then(response => {
        if (response.data.status == 200) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }
        
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'registrar', {
      id: 0,
      nombre: user.nombre,
      apellido: user.apellido,
      correo: user.correo,
      user: user.user,
      password: user.password,
      rol: user.rol
    });
  }

  registersite(sitios) {
    return axios.post(API_URL + 'sitio/registrarSitio', {
      id: 0,
      nombresitio: sitios.nombresitio,
      capacidad: sitios.capacidad,
      descripcion: sitios.descripcion
    });
  }

  email(user){
    return axios.post(API_URL + 'cambiarclave', {
      user: user.user,  
      correo: user.correo
    })
    .then(response => {
      if (response.data) {
        localStorage.setItem('user', JSON.stringify(response.data));
      }
      return response.data;
    });
  }
  
  consultarSitio() {
    return axios.get(API_URL + 'sitio/consultarSitio', {

    })
    .then(response => {
      
      return response;
    });
  }

  eliminarsitio() {
    return axios.delete(API_URL + 'sitio/eliminarsitio', {
      id: sitios.id,
      nombresitio: sitios.nombresitio,
      capacidad: sitios.capacidad,
      descripcion: sitios.descripcion

    })
    .then(response => {
      
      return response;
    });
  }
}

export default new AuthService();
