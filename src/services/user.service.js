import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:9092/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'consultarSitio', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + 'consultarUsuarios', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }
}

export default new UserService();
