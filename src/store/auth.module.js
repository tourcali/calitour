import AuthService from '../services/auth.service';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: { loggedIn: false }, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }, user) {
      return AuthService.login(user).then(
        user => {
          commit('loginSuccess', user);
          return Promise.resolve(user);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }) {
      AuthService.logout();
      commit('logout');
    },
    register({ commit }, user) {
      return AuthService.register(user).then(
        response => {
          commit('registerSuccess');
          return Promise.resolve(response.data);
        },
        error => {
          commit('registerFailure');
          return Promise.reject(error);
        }
      );
    },
    registersite({ commit }, sitios) {
      return AuthService.registersite(sitios).then(
        response => {
          commit('registersiteSuccess');
          return Promise.resolve(response.data);
        },
        error => {
          commit('registersiteFailure');
          return Promise.reject(error);
        }
      );
    },
    email({ commit }, user){
      return AuthService.email(user).then(
        response => {
          commit('emailSucess', user);
          return Promise.resolve(response.user)
        },
        error => {
          commit('emailFailure');
          return Promise.reject(error);
        }
      );
    },

    consultarSitio({ commit }){
      return AuthService.consultarSitio().then(
        response => {
          commit('consultarSitioSucess');
          return Promise.resolve(response.data)
        },
        error => {
          commit('consultarSitioFailure');
          return Promise.reject(error);
        }
      );
    },

    eliminarsitio({ commit }){
      return AuthService.eliminarsitio().then(
        response => {
          commit('eliminarsitioSucess');
          return Promise.resolve(response.data)
        },
        error => {
          commit('eliminarsitioFailure');
          return Promise.reject(error);
        }
      );
    }
  },
  mutations: {
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    registerSuccess(state) {
      state.status.loggedIn = false;
    },
    registerFailure(state) {
      state.status.loggedIn = false;
    },
    registersiteSuccess(state) {
      state.status.loggedIn = false;

    },
    registersiteFailure(state) {
      state.status.loggedIn = false;
    },
    emailSuccess(state, user) {
      state.status.loggedIn = false;
      state.user = user;
    },
    emailFailure(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    consultarSitioSucess(state) {
      state.status.loggedIn = false;

    },
    consultarSitioFailure(state) {
      state.status.loggedIn = false;

    },
    eliminarsitioSucess(state) {
      state.status.loggedIn = false;

    },
    eliminarsitioSucessFailure(state) {
      state.status.loggedIn = false;

    },
  }
};
